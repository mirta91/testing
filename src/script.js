/* mobile navigation */
  $("#nav").addClass("mobile_nav").before('<div id="menu">&#9776;</div>');
  $("#menu").click(function(){
    $("#nav").toggle();
  });
  
  $(window).resize(function(){
    if(window.innerWidth > 768) {
      $("#nav").removeAttr("style");
    }
  });
  $( "#menu" ).click(function() {
  $( ".sign-nav" ).toggle( "fast", function() {
    // Animation complete.
  });
});