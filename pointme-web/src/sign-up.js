$(document).ready(function() {

var allCountries;
var country;

    // get list of all countries and populate dropdown 
    $.getJSON('https://united-bongo-115418.appspot.com/business/countries', function(data) { 

        for(var i = 0; i < data.length; i++){
           $("#country").append('<option value="'+data[i].id+'">'+data[i].name+'</option>');
           allCountries = data;
       }


    });  

    $('.form-sign-up').submit(function(event) {
    
    // save the country which has the same id as the selected country        
        var selectedCountry = $('#country option:selected').val();

            for(var j = 0; j < allCountries.length; j++){

                if(allCountries[j].id == selectedCountry){
                    country = allCountries[j]; 
        
                }
             
    }

        // getting the form data
        var formData = {

            'name'              : $('input[name=name]').val(),
            'email'             : $('input[name=email]').val(),
            'country'           : country,
            'city'              : $('input[name=city]').val(),
            'address'           : $('input[name=address]').val(),
            'postal_numb'       : $('input[name=postal_numb]').val(),
            'phone'             : $('input[name=phone]').val(),
            'fb_url'            : $('input[name=fb_url]').val(),
            'web'               : $('input[name=web]').val(),
            'desc'              : $('textarea[name=desc]').val(),

        };

        $.ajax({
            type        : 'POST',           
            url         : 'https://united-bongo-115418.appspot.com/business/signup',        
            data        :  formData,          
            dataType    : 'json',               // type of data coming back from the server
            encode      : true,
            statusCode  : { 

                200: function() {$('.form-horizontal').append('<div class="alert alert-success" role="alert"><strong>Well done!</strong> You successfully submitted the form.</div>');},
                400: function() {},
                500: function() {}
            },
            error       : function(xhr, status, error) {

                /*error handling*/
                var jsonResponseText = $.parseJSON(xhr.responseText);

                /*name*/
                if(jsonResponseText.message.name !== undefined){
                    $('#name-group').addClass('has-error'); // add the error class to show red input
                    $('#name-label').append('<span class="help-block"> - ' + jsonResponseText.message.name + '</span>');
                }
                /*email*/
                if(jsonResponseText.message.email !== undefined){
                    $('#email-group').addClass('has-error'); // add the error class to show red input
                    $('#email-label').append('<span class="help-block"> - ' + jsonResponseText.message.email + '</span>');
                }
                    
                /*city*/
                if(jsonResponseText.message.city !== undefined){
                    $('#city-group').addClass('has-error'); // add the error class to show red input
                    $('#city-label').append('<span class="help-block"> - ' + jsonResponseText.message.city + '</span>');
                }
                
                /*address*/
                if(jsonResponseText.message.address !== undefined){
                    $('#address-group').addClass('has-error'); // add the error class to show red input
                    $('#address-label').append('<span class="help-block"> - ' + jsonResponseText.message.address + '</span>'); 
                }
               
                /*facebook*/
                if(jsonResponseText.message.fb_url !== undefined){
                    $('#face-group').addClass('has-error'); // add the error class to show red input
                    $('#face-label').append('<span class="help-block"> - ' + jsonResponseText.message.fb_url + '</span>');
                }
               
            }
            })
            // using the done promise callback
           .done(function(data) {

            });

        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();
    });

});

