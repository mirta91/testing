// include gulp
var gulp = require('gulp'); 

var changed = require('gulp-changed');
var imagemin = require('gulp-imagemin');
// include plug-ins
var minifyHTML = require('gulp-minify-html');

// include plug-ins
var jshint = require('gulp-jshint');

var concat = require('gulp-concat');
var stripDebug = require('gulp-strip-debug');
var uglify = require('gulp-uglify');

var autoprefix = require('gulp-autoprefixer');
var minifyCSS = require('gulp-minify-css');


var uncss = require('gulp-uncss');

// JS hint task
gulp.task('jshint', function() {
  gulp.src('./src/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

// minify new images
gulp.task('imagemin', function() {
  var imgSrc = './src/img/**/*',
      imgDst = './build/images';

  gulp.src(imgSrc)
    .pipe(changed(imgDst))
    .pipe(imagemin())
    .pipe(gulp.dest(imgDst));
});

// minify new or changed HTML pages
gulp.task('htmlpage', function() {
  var htmlSrc = './src/*.html',
      htmlDst = './build';

  gulp.src(htmlSrc)
    .pipe(changed(htmlDst))
    .pipe(minifyHTML())
    .pipe(gulp.dest(htmlDst));
});

// JS concat, strip debugging and minify
gulp.task('scripts', function() {
  gulp.src(['./src/lib.js','./src/*.js'])
    .pipe(concat('main.js'))
    .pipe(stripDebug())
    .pipe(uglify())
    .pipe(gulp.dest('./build/'));
});


// CSS concat, auto-prefix and minify
gulp.task('styles', function() {
  gulp.src(['./src/*.css'])
    .pipe(concat('styles.css'))
    .pipe(autoprefix('last 2 versions'))
    .pipe(minifyCSS())
    .pipe(gulp.dest('./build/'));
});

gulp.task('uncss', function() {
  return gulp.src([
  	 './src/*.css',
      'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css'
    ])
    .pipe(uncss({
      html: [
        'http://www.lay-z.net/test/index.html'
      ]
    }))
    .pipe(gulp.dest('./build'));
});


// default gulp task
gulp.task('default', ['imagemin', 'htmlpage', 'scripts', 'styles', 'uncss'], function() {
  // watch for HTML changes
  gulp.watch('./src/*.html', function() {
    gulp.run('htmlpage');
  });

  // watch for JS changes
  gulp.watch('./src/*.js', function() {
    gulp.run('jshint', 'scripts');
  });

  // watch for CSS changes
  gulp.watch('./src/*.css', function() {
    gulp.run('styles');
  });
});

